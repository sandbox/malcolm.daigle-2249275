<?php
/**
 * @file
 * Allow users to send custom metrics to the Stackdriver web service.
 */

/**
 * Implements hook_drush_command().
 */
function custom_stackdriver_metrics_drush_command() {
  $items = array();

  $items['send_stackdriver_metrics'] = array(
    'callback' => 'custom_stackdriver_metrics_send',
    'description' => 'Sends user provided metrics to Stackdriver.',
  );

  return $items;
};
