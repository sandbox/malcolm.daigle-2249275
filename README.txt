﻿-- SUMMARY -- The goal of this module is to facilitate the submission of custom
 metrics to the Stackdriver web-service. A custom hook is used to collect 
 user-defined metric names and values when a drush command is run. These names
 and values are then compiled and sent to Stackdriver.

-- REQUIREMENTS -- Drupal core 7.x, Drush 3 or later. 
 To use this module you will need two additional things, a Stackdriver account
 and a Stackdriver API Key generated specifically for you Drupal installation.
 To create a stackdriver account, simply go to:
 https://www.stackdriver.com/signup/ 
 and sign up for an account. Once you have an account, 
 go to https://app.stackdriver.com/settings/apikeys to create a new API key 
 or to manage existing keys. You will need to use a Metric Data type Key in 
 order for this module to work.

-- INSTALLATION -- Install as usual, directions at:
 https://drupal.org/documentation/install/modules-themes/modules-7

-- CONFIGURATION -- Configure your Stackdriver API Key at:
 Administration >> Admin Index >> Custom Stackdriver Metrics

-- USAGE -- 
 This module uses a custom hook called hook_stackdriver_metrics. The best way
 to learn how to use this hook is through an example:
 Say I have a module called 'Commerce'. 'Commerce' keeps track of the total 
 numbers of orders that have been processed by my site on any given day. I want
 to send this metric to Stackdriver so I can measure my site's commerce 
 traffic. I have already followed the steps to install and configure the Custom
 Stackdriver Metrics module.

 Within my Commerce.module file, I will place the following code:

 Commerce_stackdriver_metrics() {
	$items = array();
	$items['total_orders'] = array(
		'name' = 'total_orders',
		'value' = total_orders()
	);
	return $items;
 }

 The name of my module, 'Commerce', replaces the word 'hook' in the hook. The
 name of the metric I am sending is 'total_orders' so I make this the key for
 the metric in the $items array. I also set the value of 'name' to
 'total_orders'. I set the value of 'value' to total_orders(), where the
 function total_orders() would return the total number of orders processed.

 To send another metric from this module, all I would have to do is add another
 metric to the $items array following the same format I used for total_orders.

 Later, when I want to send this metric to stackdriver, all I have to do is run
 the command: drush send_stackdriver_metrics.

 This drush command will invoke all of the hook_stackdriver_metric hooks, 
 compile the names and values they return, and send these metrics to 
 Stackdriver.

 Additionally, If I wanted to automate the metric sending process, I could
 configure this drush command to run on cron using drupal's included cron
 service or a third party cron module.
